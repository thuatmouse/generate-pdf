# generate-pdf

Use puppeteer library to generate pdf.

<!-- toc -->

- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installing](#installing)
  - [Start server](#start-server)
- [APIs](#apis)
- [Sample](#sample)

<!-- tocstop -->

## Getting Started

### Prerequisites
- [`yarn`](https://yarnpkg.com/) or [`npm`]

### Installing
1. Clone project
  ```bash
   git clone https://gitlab.com/thuatmouse/generate-pdf.git
   cd generate-pdf
  ```
2. Setup .env & Install dependencies

   ```bash
   yarn setup
   ```
   Or
   - create .env & copy from .env.template file
   - yarn install

### Start server
  ```bash
  yarn dev
  ```
   runs on [port 3000](http://localhost:3000).

### APIs
- [`GET`] /status
- [`GET`] /sample/download/chart-image
- [`GET`] /sample/view/pdf
### Sample
  Open browser and access url
  - http://localhost:3000/status
  - http://localhost:3000/sample/download/chart-image
  - http://localhost:3000/sample/view/pdf
