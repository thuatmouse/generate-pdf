const puppeteer = require("puppeteer");
const GoogleChartsNode = require("google-charts-node");
const drawChart = require("./drawChart");
const { propertyFields, propertyValues } = require("../mock/property");

class GenerateSampleChart {
  chartImage = async () => {
    // Render the chart to image
    const imageBuffer = await GoogleChartsNode.render(drawChart);
    return imageBuffer;
  };

  chartPdf = async () => {
    const browser = await puppeteer.launch({
      args: ["--no-sandbox", "--disable-setuid-sandbox"],
    });

    // Open a new page with the headless browser
    const page = await browser.newPage();

    const htmlContent = await buildHTMLContent(this);

    // Route the headless browser to the webpage for printing
    await page.setContent(htmlContent);

    await page.addStyleTag({
      content: `
      .a4 {
        height: calc(11.7in + 26px);
        display: block;
      }
      .table {
        overflow: hidden;
      }
    `,
    });

    // Print the page as pdf
    const buffer = await page.pdf({
      displayHeaderFooter: true,
      footerTemplate: `
          <div style="color: lightgray; border-top: solid lightgray 1px; font-size: 10px; padding-top: 5px; text-align: center; width: 100%;">
            <span class="pageNumber"></span>
          </div>
        `,
      margin: {
        top: 40,
        bottom: 40,
        left: 40,
        right: 40,
      },
      format: "A4",
    });

    // Close the headless browser
    browser.close();

    // send the pdf
    return buffer;
  };
}

const buildHTMLContent = async (instance) => {
  const imageBuffer = await instance.chartImage();

  const imageBase64 = Buffer.from(imageBuffer).toString("base64");

  const tableHeaders = getPropertyTableHeaders();
  const tableRows = getPropertyTableValues();

  return `
      <!-- content page 1 -->
      <div id="page-1" class="a4 chart">
        <img src="data:image/png;base64,${imageBase64}" />
      </div>

      <!-- content page 2 -->
      <div id="page-2" class="a4 table">
        <table border="1">
          <thead>
            <th>STT</th>
            ${tableHeaders}
          </thead>
          <tbody>
            ${tableRows}
          </tbody>
        </table>
      </div>

      <!-- content page 3 -->
      <div id="page-3" class="a4 image">
        <img src="https://api.timeforkids.com/wp-content/uploads/2019/09/final-cover-forest.jpg" width="100%" height="auto" />
      </div>
    `;
};

const getPropertyTableHeaders = () => {
  return propertyFields.reduce((thTableString, field) => {
    thTableString += `<th>${field}</th>\n`;
    return thTableString;
  }, "");
};

const getPropertyTableValues = () => {
  return propertyValues.reduce((thTableString, property, index) => {
    thTableString += `<tr>\n`;
    thTableString += `\t<td>${index + 1}</td>\n`;

    propertyFields.forEach((field) => {
      const value = property[field];
      thTableString += `\t<td>${value}</td>\n`;
    });
    thTableString += `</tr>\n`;
    return thTableString;
  }, "");
};

module.exports = new GenerateSampleChart();
