require("dotenv").config();
const express = require("express");
const generateSampleChart = require("./helpers/GenerateSampleChart");

const app = express();

app.get("/status", (req, res, next) => {
  return res.sendStatus(200);
});

app.get("/sample/download/chart-image", async (req, res, next) => {
  const imageBuffer = await generateSampleChart.chartImage();

  res.setHeader("Content-Disposition", `attachment; filename="chart.png"`);
  res.send(imageBuffer);
});

app.get("/sample/view/pdf", async (req, res, next) => {
  const filename = await generateSampleChart.chartPdf();

  res.setHeader("Content-Type", "application/pdf");

  return res.send(filename);
});

const host = process.env.HOST || "localhost";
const port = process.env.PORT || 3000;

app.listen(port, host, () => {
  console.log(`Server listening on http://${host}:${port}`);
});
